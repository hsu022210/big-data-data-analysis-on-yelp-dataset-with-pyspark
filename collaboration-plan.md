# Project 3: Collaboration Plan

Group Members:
* Melanie Baybay
* Aishwayra Chanrashekhar
* Alec Hsu
* Chaitanya Mattey

## Dataset

Our group plans to analyze the Yelp Open Dataset available here: [Yelp Challenge Dataset](https://www.yelp.com/dataset/challenge)

This dataset includes: 
* **business data**: location, attributes, and categories.
* **reviews**: full review text data including the user_id that wrote the review and the business_id the review is written for
* **users**: each user's friend mapping and all the metadata associated with the user.
* **checkins**: user check-ins on a business.
* **tips**: quick suggestions written by a user on a business.
* **photos_aux**: photo-id, business id, caption, labels
* **photos**: user-submitted photos, with photo-id as mentioned above. 

## Analysis Plan

We aim to answer the following questions with this dataset: 
* How does business popularity relate to its distance from local landmarks (e.g. Golden Gate Park, Twin Peaks)?
* How does an influential user affect the check-in rate of a business?
* Analyze user's review type based on their review feedback.
* Sentiment analysis and classification of user reviews.

